const backendEndpoint = 'http://localhost:8081'
export const endpointUrls = {
    dealEndpoint: backendEndpoint + `/deal`,
    loginEndpoint: backendEndpoint + `/login`,
    ctpyEndpoint: backendEndpoint + `/ctpy/tradecount`
}