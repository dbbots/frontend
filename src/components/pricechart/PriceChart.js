import React, { useState } from 'react';
import { withLatestFrom, map } from 'rxjs/operators'
import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { Line } from 'react-chartjs-2';
import { endpointUrls } from '../../config'

const dealObservable = Observable.create(observer => {
  const source = new EventSource(endpointUrls.dealEndpoint);
  source.addEventListener('message', (messageEvent) => {
    console.log(messageEvent);
    observer.next(messageEvent.data);
  }, false);
});

function range(end) {
  var ans = [];
  for (let i = 1; i <= end; i++) {
    ans.push(i);
  }
  return ans;
}

function PriceChart() {

  let instruments = [
    {
      "instrumentName": "Astronomica",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Borealis",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Celestial",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Deuteronic",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Eclipse",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Floral",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Galactia",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Heliosphere",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Interstella",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Jupiter",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Koronis",
      "buyPrices": [], "sellPrices": []
    },
    {
      "instrumentName": "Lunatic",
      "buyPrices": [], "sellPrices": []
    }]
  const [instrumentsList, setInstrumentsList] = useState(instruments)
  const [currentInstrument, setCurrentInstrument] = useState("Astronomica");

  const pushByInstrumentName = (instrumentName, price, type) => {
    let updatedInstrumentList = instrumentsList
    updatedInstrumentList.forEach((instrument) => {
      if (instrument.instrumentName === instrumentName) {
        if (type === "B") {
          instrument.buyPrices.unshift(price);
          if (instrument.buyPrices.length >= 1000) {
            instrument.buyPrices.pop();
          }
        } else {
          instrument.sellPrices.unshift(price);
          if (instrument.sellPrices.length >= 1000) {
            instrument.sellPrices.pop();
          }
        }
      }
    })
    setInstrumentsList(updatedInstrumentList)
  }


  useObservable(
    state =>
      dealObservable.pipe(
        withLatestFrom(state),
        map(([state]) => {
          console.log('Price is here!')
          console.log(state)
          let priceObj = state.data
          pushByInstrumentName(priceObj.instrumentName, priceObj.price, priceObj.type)
          return state;
        })
      )
  );

  //arrays of strings of unparsed JSON
  const priceArray = [
    '{"data": {"instrumentName": "Astronomica", "price": 23.44999694824219, "type": "B", "date": "2019-02-02", "time": "10:10:10"}}'
  ]

  const currentBuyList = () => {
    return instrumentsList.find((value) => { return value.instrumentName === currentInstrument }).buyPrices
  }
  const currentSellList = () => {
    return instrumentsList.find((value) => { return value.instrumentName === currentInstrument }).sellPrices
  }

  //customize the colors
  const data = {
    labels: range(currentBuyList.length), //time 
    datasets: [
      {
        label: 'Buy Prices',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: currentBuyList //price
      },

      {
        label: 'Sell Prices',
        // fill: false,
        // lineTension: 0.1,
        // backgroundColor: 'rgba(75,192,192,0.4)',
        // borderColor: 'rgba(75,192,192,1)',
        // borderCapStyle: 'butt',
        // borderDash: [],
        // borderDashOffset: 0.0,
        // borderJoinStyle: 'miter',
        // pointBorderColor: 'rgba(75,192,192,1)',
        // pointBackgroundColor: '#fff',
        // pointBorderWidth: 1,
        // pointHoverRadius: 5,
        // pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        // pointHoverBorderColor: 'rgba(220,220,220,1)',
        // pointHoverBorderWidth: 2,
        // pointRadius: 1,
        // pointHitRadius: 10,
        data: currentSellList //price
      }
    ]
  };

  let createSelectItems = () => {
    console.log("I AM HEERERER", instruments[0])
    let items = [];
    for (let i = 0; i <= instruments; i++) {
      console.log(instruments[i])
      items.push(<option value={instruments[i].instrumentName}>{instruments[i].instrumentName}</option>);
    }
    return items;
  }

  function onDropdownSelected(e) {
    console.log("THE VAL", e.target.value);
    let val = e.target.value;
    setCurrentInstrument(val)
    //here you will see the current selected value of the select input
}

  return (
    <div>
      <h2>Price Chart</h2>
      <select onChange={onDropdownSelected} value={currentInstrument}>
        {createSelectItems()}
      </select>
      <Line data={data} />
    </div>
  );

}


export default PriceChart;