import Table from 'react-bootstrap/Table'
import React from 'react';


function ProfitLossTable(){

  var instrument_array = [
    `{"instrumentName": "Astronomica", "realProfit": -979392.73, "effProfit": -2854280.54}`, 
    `{"instrumentName": "Borealis", "realProfit": -30728557.31, "effProfit": -67398532.86}`, 
 `{"instrumentName": "Celestial", "realProfit": -27808.48, "effProfit": 778747.03}`, 
 `{"instrumentName": "Deuteronic", "realProfit": 1746444.23, "effProfit": 3119003.61}`,
 `{"instrumentName": "Eclipse", "realProfit": 2293522.77, "effProfit": 3970309.23}`, 
 `{"instrumentName": "Floral", "realProfit": -1708228.54, "effProfit": -30305573.93}`, 
 `{"instrumentName": "Galactia", "realProfit": -52695872.54, "effProfit": -87420209.79}`, 
 `{"instrumentName": "Heliosphere", "realProfit": -12047011.01, "effProfit": -22588833.04}`,
 `{"instrumentName": "Interstella", "realProfit": -13220759.75, "effProfit": -39469189.09}`, 
 `{"instrumentName": "Jupiter", "realProfit": -21287554.63, "effProfit": -59811192.84}`, 
 `{"instrumentName": "Koronis", "realProfit": 5963528.21, "effProfit": 23580398.58}`, 
 `{"instrumentName": "Lunatic", "realProfit": 6575137.68, "effProfit": 33049968.57}`
  ]

  var parsedArray = []
  //console.log(JSON.parse(instrument_array[0]));
  function parseJSON (targetarray, jsonarray) {
    for (var i = 0; i < jsonarray.length; i++){
      targetarray.push(JSON.parse(jsonarray[i]));
    }
  }

  parseJSON(parsedArray, instrument_array);
  console.log(parsedArray[0].instrumentName);

  var realProfitArr = [];
  var effProfitArr = [];

  function separate(realarray, effarray, jsonarray) {
    for (var i = 0; i < jsonarray.length; i++){
      realarray.push(jsonarray[i].realProfit);
      effarray.push(jsonarray[i].effProfit)
    }
  }

  separate(realProfitArr, effProfitArr, parsedArray);

    return(
    <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>Instrument Name</th>
      <th>Effective Profit/Loss</th>
      <th>Realised Profit/Loss</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>{parsedArray[0].instrumentName}</td>
      <td>{realProfitArr[0]}</td>
      <td>{effProfitArr[0]}</td>
    </tr>
    <tr>
    <td>2</td>
      <td>{parsedArray[1].instrumentName}</td>
      <td>{realProfitArr[1]}</td>
      <td>{effProfitArr[1]}</td>
    </tr>
    <tr>
    <td>3</td>
      <td>{parsedArray[2].instrumentName}</td>
      <td>{realProfitArr[2]}</td>
      <td>{effProfitArr[2]}</td>
    </tr>
    <tr>
    <td>4</td>
      <td>{parsedArray[3].instrumentName}</td>
      <td>{realProfitArr[3]}</td>
      <td>{effProfitArr[3]}</td>
    </tr>
    <tr>
    <td>5</td>
      <td>{parsedArray[4].instrumentName}</td>
      <td>{realProfitArr[4]}</td>
      <td>{effProfitArr[4]}</td>
    </tr>
    <tr>
    <td>6</td>
      <td>{parsedArray[5].instrumentName}</td>
      <td>{realProfitArr[5]}</td>
      <td>{effProfitArr[5]}</td>
    </tr>
    <tr>
    <td>7</td>
      <td>{parsedArray[6].instrumentName}</td>
      <td>{realProfitArr[6]}</td>
      <td>{effProfitArr[6]}</td>
    </tr>
    <tr>
    <td>8</td>
      <td>{parsedArray[7].instrumentName}</td>
      <td>{realProfitArr[7]}</td>
      <td>{effProfitArr[7]}</td>
    </tr>
    <tr>
    <td>9</td>
      <td>{parsedArray[8].instrumentName}</td>
      <td>{realProfitArr[8]}</td>
      <td>{effProfitArr[8]}</td>
    </tr>
    <tr>
    <td>10</td>
      <td>{parsedArray[9].instrumentName}</td>
      <td>{realProfitArr[9]}</td>
      <td>{effProfitArr[9]}</td>
    </tr>
    <tr>
    <td>11</td>
      <td>{parsedArray[10].instrumentName}</td>
      <td>{realProfitArr[10]}</td>
      <td>{effProfitArr[10]}</td>
    </tr>
    <tr>
    <td>12</td>
      <td>{parsedArray[11].instrumentName}</td>
      <td>{realProfitArr[11]}</td>
      <td>{effProfitArr[11]}</td>
    </tr>
    
  </tbody>
</Table>)

}

export default ProfitLossTable;
