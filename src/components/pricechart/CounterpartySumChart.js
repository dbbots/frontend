import React, { useState } from 'react';
import { Polar } from 'react-chartjs-2';
import {endpointUrls} from '../../config'

function CounterpartySumChart() {

  function parseJSON(targetarray, jsonarray) {
    for (var i = 0; i < jsonarray.length; i++){
      targetarray.push(JSON.parse(jsonarray[i]));
    }
  }
  //const [priceArray, setPriceArray] = useState([]);
  var [counterpartyArray, setCounterpartyArray] = useState([]);
  
  function uploadData() {
    fetch(endpointUrls.ctpyEndpoint, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then((response) => {
        return response.json()
    }).then((json) => {
        let jsonResponse = json
        console.log(jsonResponse.data)
        setCounterpartyArray(jsonResponse.data)
    })
    .catch((error) => {
        console.log(error);
    })
}
uploadData();

  var counterparty = [
    `{"data": [{
              "ctpy": "John",
              "ctpy_count": 7
          },
          {
              "ctpy": "Lewis",
              "ctpy_count": 6
          },
          {
              "ctpy": "Lina",
              "ctpy_count": 7
          },
          {
              "ctpy": "Nidia",
              "ctpy_count": 8
          },
          {
              "ctpy": "Richard",
              "ctpy_count": 11
          },
          {
              "ctpy": "Selvyn",
              "ctpy_count": 8
          }
      ]}`
    ]

    var parsedArray = []
    parseJSON(parsedArray, counterparty);
    //console.log(parsedArray[0].data[0].ctpy)

    var ctpy_name = []
    for (var i = 0; i < 6; i ++){
      ctpy_name.push(parsedArray[0].data[i].ctpy)
    }

    console.log(ctpy_name)

    var ctpy_count = []
    for (var j = 0; j < 6; j++){
      ctpy_count.push(parsedArray[0].data[j].ctpy_count)
    }
  

  //customize the colors
  const data = {
    labels: counterpartyArray.map((value)=>{return value.ctpy}), //time 
    datasets: [{
      label: 'Instrument Sums',
      data: counterpartyArray.map((value)=>{return value.ctpy_count}), //price
      backgroundColor: [
        '#FF6384',
        '#4BC0C0',
        '#FFCE56',
        '#E7E9ED',
        '#36A2EB',
        "#FF8C00"
      ]
    }
    ]
  };

  return (
    <div>
      <h2>Counterparty Chart</h2>
      <Polar data={data} />
    </div>
  );

}

export default CounterpartySumChart;