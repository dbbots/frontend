import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom'
import Alert from 'react-bootstrap/Alert'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import CounterpartySumChart from './CounterpartySumChart';
import PriceChart from './PriceChart'
import TraderTable from './tradertable'

function Dashboard() {
    const [isTokenPresents, setTokenPresents] = useState(window.localStorage.getItem("JWT"))

    if (!isTokenPresents) {
        return (
            <Alert variant="danger" onClose={() => { return <Redirect to='/login' /> }} dismissible>
                <Alert.Heading>Sorry, you not allowed to see that page</Alert.Heading>
                <p>
                    <Link to={`/login`}>Back to login page</Link>
                </p>
            </Alert>
        )
    }

    return (
        <Container>
            <Col>
                    {/* <PriceChart /> */}
                    <p></p>
                    <CounterpartySumChart />
                    <p></p>
                    {/* <TraderTable /> */}
            </Col>
        </Container>
    )
}

export default Dashboard;