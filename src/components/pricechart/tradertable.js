import Table from 'react-bootstrap/Table'
import React from 'react';


function TraderTable(){

  var instrument_array = [
    '{"instrumentName": "Astronomica", "endPosition": 265.0}', '{"instrumentName": "Borealis", "endPosition": 5183.0}',
    '{"instrumentName": "Celestial", "endPosition": -114.0}', '{"instrumentName": "Deuteronic", "endPosition": -194.0}',
    '{"instrumentName": "Eclipse", "endPosition": -237.0}', '{"instrumentName": "Floral", "endPosition": 4042.0}',
    '{"instrumentName": "Galactia", "endPosition": 4908.0}', '{"instrumentName": "Heliosphere", "endPosition": 1490.0}',
    '{"instrumentName": "Interstella", "endPosition": 3710.0}', '{"instrumentName": "Jupiter", "endPosition": 5445.0}',
    '{"instrumentName": "Koronis", "endPosition": -2490.0}', '{"instrumentName": "Lunatic", "endPosition": -3742.0}'
  ]

  var parsedArray = []
  //console.log(JSON.parse(instrument_array[0]));
  function parseJSON (targetarray, jsonarray) {
    for (var i = 0; i < jsonarray.length; i++){
      targetarray.push(JSON.parse(jsonarray[i]));
    }
  }

  parseJSON(parsedArray, instrument_array);
  console.log(parsedArray[0].instrumentName);


    return(
    <Table striped bordered hover>
  <thead>
    <tr>
      <th>#</th>
      <th>Instrument Name</th>
      <th>End Position</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>{parsedArray[0].instrumentName}</td>
      <td>{parsedArray[0].endPosition}</td>
    </tr>
    <tr>
      <td>2</td>
      <td>{parsedArray[1].instrumentName}</td>
      <td>{parsedArray[1].endPosition}</td>
    </tr>
    <tr>
      <td>3</td>
      <td>{parsedArray[2].instrumentName}</td>
      <td>{parsedArray[2].endPosition}</td>
    </tr>
    <tr>
      <td>4</td>
      <td>{parsedArray[3].instrumentName}</td>
      <td>{parsedArray[3].endPosition}</td>
    </tr>
    <tr>
      <td>5</td>
      <td>{parsedArray[4].instrumentName}</td>
      <td>{parsedArray[4].endPosition}</td>
    </tr>
    <tr>
      <td>6</td>
      <td>{parsedArray[5].instrumentName}</td>
      <td>{parsedArray[5].endPosition}</td>
    </tr>
    <tr>
      <td>7</td>
      <td>{parsedArray[6].instrumentName}</td>
      <td>{parsedArray[6].endPosition}</td>
    </tr>
    <tr>
      <td>8</td>
      <td>{parsedArray[7].instrumentName}</td>
      <td>{parsedArray[7].endPosition}</td>
    </tr>
    <tr>
      <td>9</td>
      <td>{parsedArray[8].instrumentName}</td>
      <td>{parsedArray[8].endPosition}</td>
    </tr>
    <tr>
      <td>10</td>
      <td>{parsedArray[9].instrumentName}</td>
      <td>{parsedArray[9].endPosition}</td>
    </tr>
    <tr>
      <td>11</td>
      <td>{parsedArray[10].instrumentName}</td>
      <td>{parsedArray[10].endPosition}</td>
    </tr>
    <tr>
      <td>12</td>
      <td>{parsedArray[11].instrumentName}</td>
      <td>{parsedArray[11].endPosition}</td>
    </tr>
  </tbody>
</Table>)

}

export default TraderTable;
