import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Alert from 'react-bootstrap/Alert'
import React from 'react';
import { useState } from 'react';
import { endpointUrls } from '../../config';
import { Redirect } from 'react-router-dom';

function Login() {

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [backendError, setBackendError] = useState(false);
    const [loginApproved, setLoginApproved] = useState(false);
    const applyCredentials = event => {
        event.preventDefault()
        console.log(`Here is a query with your credentials: ${login} and ${password}`)
        fetch(endpointUrls.loginEndpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                username: login,
                password: password,
            }),
        }).then((response) => {
            console.log(response)
            return response.json()
        }).then((json) => {
            let jsonResponse = json
            console.log(jsonResponse);
            if (jsonResponse.status === "success") {
                window.localStorage.setItem('JWT', jsonResponse.auth_token)
                setLoginApproved(true);
            }
        })
            .catch((error) => {
                console.log(error);
                setBackendError(true);
            })
    }

    const updateLogin = event => {
        setLogin(event.target.value)
    }
    const updatePassword = event => {
        setPassword(event.target.value)
    }

    if (backendError) {
        return (
            <Alert variant="danger" onClose={() => setBackendError(false)} dismissible>
                <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                <p>
                    Please try again. If it not goes away, please notify developers about this error by tapping on {''}
                    <Alert.Link href="#">this link</Alert.Link>. We're really sorry!
                            </p>
            </Alert>
        )
    }

    if (loginApproved) {
        return (
            <Redirect to='/dashboard' />
        )
    }

    return (
        <>
            <h4 style={{ textAlign: "center" }}>
                Welcome to our new modern Data Visualization Application
        </h4>

            <Form onSubmit={applyCredentials}>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Login</Form.Label>
                    <Form.Control type="text" placeholder="Enter login" value={login} onChange={updateLogin} />
                    <Form.Text className="text-muted">
                        Put the best trader name in here (or not yours).
                </Form.Text>
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={updatePassword} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
            </Button>
            </Form>
        </>
    );
}

export default Login;