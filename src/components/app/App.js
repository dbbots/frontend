import React from 'react';
import Login from '../login/Login'
import Dashboard from '../pricechart/Dashboard'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import dblogo from '../../assets/images/dblogo.png'

function App() {
  return (
    <>
      <Router>
        <Navbar bg="dark" variant="dark" style={{ marginBottom: 10 }}>
          <Navbar.Brand>
            <img
              alt=""
              src={dblogo}
              width="40"
              height="40"
              style={{ marginRight: 10 }}
            />
            {'App Improvements Project'}
          </Navbar.Brand>
        </Navbar>
        <Switch>
          <Redirect exact from="/" to="login" />
          <Container>
            <Row>
              <Col xs={1} md={2} ></Col>
              <Col xs={10} md={8}>
                <Route path='/login' exact render={() => <Login />} />
                <Route path='/dashboard' render={() =>   <Dashboard />}/>
              </Col>
              <Col xs={1} md={2} ></Col>
            </Row>
          </Container>
        </Switch>
      </Router>
    </>
  );
}

export default App;
